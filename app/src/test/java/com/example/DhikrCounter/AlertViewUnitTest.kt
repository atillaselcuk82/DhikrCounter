package com.example.DhikrCounter

import com.example.DhikrCounter.ui.counter.InputValidationHelper.isInputCorrect
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class AlertViewUnitTest {

    @Test
    fun emptyInput() {
        val actual = isInputCorrect("", "")
        val expected = false
        assertEquals(expected, actual)
    }

    @Test
    fun emptyName() {
        val actual = isInputCorrect("", "3")
        val expected = false
        assertEquals(expected, actual)
    }

    @Test
    fun emptyAmount() {
        val actual = isInputCorrect("Atilla", "")
        val expected = false
        assertEquals(expected, actual)
    }

    @Test
    fun amountIsChar() {
        val actual = isInputCorrect("Atilla","a")
        val expected = false
        assertEquals(expected, actual)
    }

    @Test
    fun amountIsString() {
        val actual = isInputCorrect("Atilla", "aaa")
        val expected = false
        assertEquals(expected, actual)
    }

    @Test
    fun amountIsNegativeDigit() {
        val actual = isInputCorrect("Atilla", "-1")
        val expected = false
        assertEquals(expected, actual)
    }

    @Test
    fun amountIsZero() {
        val actual = isInputCorrect("Atilla", "0")
        val expected = false
        assertEquals(expected, actual)
    }

    @Test
    fun amountIsPositiveDigit() {
        val actual = isInputCorrect("Atilla", "1")
        val expected = true
        assertEquals(expected, actual)
    }
}
