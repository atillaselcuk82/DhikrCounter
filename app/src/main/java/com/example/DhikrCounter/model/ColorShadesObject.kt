package com.example.DhikrCounter.model

object ColorShadesObject {

    private var colors = arrayListOf(
        "Red",
        "Pink",
        "Indigo",
        "Blue",
        "Cyan",
        "Green",
        "Yellow"
    )

    private var shades = arrayListOf("dark", "medium", "light")

    var colorShadeList = arrayListOf<ColorShade>()

    fun createColorShadeList(): ArrayList<ColorShade> {
        colors.forEach { c ->
            shades.forEach { s ->
                colorShadeList.add(ColorShade(c, s))
            }
        }
        return colorShadeList
    }

    fun getXmlPrimary(color: String, shade: String): String {
        return "$shade${color}PrimaryColor"
    }
}