package com.example.DhikrCounter.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class ColorShade (val color: String = "App",
                  val shade: String = "Theme"): Parcelable

