package com.example.DhikrCounter.model

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DhikrDataManager(val context: Context) {

    private val gson = Gson()

    private fun sharedPrefs() = PreferenceManager.getDefaultSharedPreferences(context)

    fun saveDhikr(dhikr: Dhikr) {
        val oldList = readDhikrs()
        val newList = oldList + dhikr
        sharedPrefs().edit().putString("DHIKRLIST", gson.toJson(newList)).apply()
    }

    fun readDhikrs(): List<Dhikr> {
        val list = sharedPrefs().getString("DHIKRLIST", "")
        val listType = object : TypeToken<List<Dhikr>>() { }.type
        val newList = gson.fromJson<List<Dhikr>>(list, listType)
        return newList ?: emptyList()
    }

    fun editDhikr(dhikr: Dhikr) {
        val list: MutableList<Dhikr> = readDhikrs() as MutableList<Dhikr>
            list.forEachIndexed { index, item ->
                if (item.id == dhikr.id) {
                    list[index] = dhikr
            }
        }
        saveDhikrs(list)
    }

    fun saveDhikrs(list: List<Dhikr>) {
        sharedPrefs().edit().putString("DHIKRLIST", gson.toJson(list)).apply()
    }

    fun deleteDhikr(dhikr: Dhikr) {
        val oldList = readDhikrs()
        val newList = oldList - dhikr
        sharedPrefs().edit().putString("DHIKRLIST", gson.toJson(newList)).apply()
    }

    fun saveTheme(theme: ColorShade) {
        sharedPrefs().edit().putString("THEME", gson.toJson(theme)).apply()
    }

    fun readTheme(): ColorShade {
        val json = sharedPrefs().getString("THEME", "")
        return if (json.isNotEmpty()) {
            gson.fromJson(json, ColorShade::class.java)
        } else {
            ColorShade()
        }
    }
}


