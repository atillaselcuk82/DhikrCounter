package com.example.DhikrCounter.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Dhikr(var name: String,
                 var description: String?,
                 var amount: Int,
                 var alert: Int?,
                 var date: Date?,
                 var counted: Int = 0,
                 var total: Int = 0,
                 var id: String = UUID.randomUUID().toString()): Parcelable

