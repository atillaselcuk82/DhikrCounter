package com.example.DhikrCounter.ui.themes

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.DhikrCounter.R
import com.example.DhikrCounter.model.ColorShadesObject.createColorShadeList
import kotlinx.android.synthetic.main.fragment_themes.*

class ThemesFragment: Fragment() {

    val adapter = ThemesAdapter(createColorShadeList())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(container?.context).inflate(R.layout.fragment_themes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        themesRecyclerView.layoutManager = GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false)
        themesRecyclerView.adapter = adapter

        themesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                adapter.scrollDirection = if (dy > 0) {
                    ThemesAdapter.ScrollDirection.DOWN
                } else {
                    ThemesAdapter.ScrollDirection.UP
                }
            }
        })
    }
}

