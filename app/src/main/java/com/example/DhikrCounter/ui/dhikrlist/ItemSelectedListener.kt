package com.example.DhikrCounter.ui.dhikrlist

interface ItemSelectedListener {
    fun onItemSelected()
    fun onItemCleared()
}