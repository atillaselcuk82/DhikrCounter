package com.example.DhikrCounter.ui.counter

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.DhikrCounter.R
import com.example.DhikrCounter.model.Dhikr
import com.example.DhikrCounter.model.DhikrDataManager
import com.example.DhikrCounter.ui.counter.InputValidationHelper.isInputCorrect
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.set_dhikr_alert_dialog.view.*

class CounterFragment: Fragment() {

    private var dhikr: Dhikr? = null
    private var counter = 0
    private var totalTimes = 0
    private var amount = 0
    private var name: String? = null

    companion object {

        const val DHIKR_LIST_KEY = "DHIKR_LIST"
        const val COUNTER_KEY = "COUNTER_KEY"
        const val TITLE_KEY = "TITLE_KEY"
        const val AMOUNT_KEY = "AMOUNT_KEY"
        const val TOTAL_TIMES_KEY = "TOTAL_TIMES_KEY"

        fun newInstance(dhikr: Dhikr): CounterFragment = CounterFragment().apply {
            arguments = Bundle().apply {
                putParcelable(DHIKR_LIST_KEY, dhikr)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        count_btn.setOnClickListener { keepCount() }
        reset_btn.setOnClickListener { reset() }
        set_btn.setOnClickListener { setDhikr() }

        arguments?.let{ args ->
            reset_btn.visibility = View.INVISIBLE
            set_btn.visibility = View.INVISIBLE
            dhikr = args.getParcelable(DHIKR_LIST_KEY)
            dhikr?.let {
                setUIValues(it.name, it.amount, it.counted, it.total)
            }
            savedInstanceState?.let { bundle ->
                counter = bundle.getInt(COUNTER_KEY)
                name = bundle.getString(TITLE_KEY)
                amount = bundle.getInt(AMOUNT_KEY)
                totalTimes = bundle.getInt(TOTAL_TIMES_KEY)
                restoreValues()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(COUNTER_KEY, counter)
        name?.let {
            outState.putString(TITLE_KEY, it)
            outState.putInt(AMOUNT_KEY, amount)
            outState.putInt(TOTAL_TIMES_KEY, totalTimes)
        }
    }

    private fun restoreValues() {
        count_btn.text = counter.toString()
        name?.let {
            setUIValues(it, amount, counter, totalTimes)
        }
    }

    private fun keepCount() {
        if (name != null) {
            counter = (++counter).rem(amount)
            count_btn.text = "$counter"
            amount_tv.text = getString(R.string.left, (amount - counter))
            if (counter == 0) {
                ++totalTimes
                total_times_tv.text = getString(R.string.total, totalTimes)
            }
            dhikr?.let {dhikr ->
                dhikr.counted = counter
                dhikr.total = totalTimes
                activity?.let {
                    DhikrDataManager(it.applicationContext).editDhikr(dhikr)
                }
            }
        } else {
            count_btn.text = "${++counter}"
        }
    }

    private fun setUIValues(name: String, amount: Int, counter: Int, totalTimes: Int) {
        this.name = name
        this.amount = amount
        this.counter = counter
        this.totalTimes = totalTimes
        dhikr_title_tv.text = name
        amount_tv.text = getString(R.string.left, (amount - counter))
        count_btn.text = "$counter"
        total_times_tv.text = getString(R.string.total, totalTimes)
    }

    private fun setDhikr() {
        val dialogView = layoutInflater.inflate(R.layout.set_dhikr_alert_dialog,null)
        val builder = AlertDialog.Builder(context)
            .setView(dialogView)
            .setTitle(getString(R.string.set_dhikr_dialog_title))
        val alertDialog = builder.show()

        setButton(dialogView, alertDialog)
        cancelButton(dialogView, alertDialog)
    }

    private fun setButton(dialogView: View, alertDialog: AlertDialog) {
        dialogView.dialog_set_btn.setOnClickListener {
            val name = dialogView.name_et.text.toString()
            val amount = dialogView.amount_et.text.toString()

            if(isInputCorrect(name, amount)) {
                reset()
                alertDialog.dismiss()
                setUIValues(name, amount.toInt(), counter, totalTimes)
            } else {
                Toast.makeText(context, getString(R.string.incorrect_input_warning), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun cancelButton(dialogView: View, alertDialog: AlertDialog) {
        dialogView.cancel_btn.setOnClickListener{
            alertDialog.dismiss()
        }
    }

    private fun reset() {
        resetBindings()
        resetTexts()
    }

    private fun resetBindings() {
        name = null
        counter = 0
        totalTimes = 0
        amount = 0
    }

    private fun resetTexts() {
        count_btn.text = counter.toString()
        amount_tv.text = getString(R.string.empty_textview)
        total_times_tv.text = getString(R.string.empty_textview)
        dhikr_title_tv.text = getString(R.string.app_name)
    }
}