package com.example.DhikrCounter.ui

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.DhikrCounter.R
import com.example.DhikrCounter.model.DhikrDataManager
import com.example.DhikrCounter.model.Dhikr
import com.example.DhikrCounter.ui.counter.CounterFragment
import com.example.DhikrCounter.ui.dhikrlist.DhikrListFragment
import com.example.DhikrCounter.ui.dhirkform.DhikrFormFragment
import com.example.DhikrCounter.ui.themes.ThemesFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    DhikrListFragment.OnFragmentInteractionListener {

    lateinit var fragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        DhikrDataManager(this).readTheme().let {
            theme.applyStyle(styleId(it.color, it.shade), true)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        if (savedInstanceState == null) {
            displayFragment(-1)
        }
    }

    private fun styleId(color: String, shade: String): Int {
        return this.resources.getIdentifier(
            "$shade$color", "style", this.packageName
        )
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun displayDhikrInCounter(dhikr: Dhikr) {
        CounterFragment.newInstance(dhikr).let {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, it, getString(R.string.home_fragment_tag))
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onDhikrListItemClicked(dhikr: Dhikr) {
        displayDhikrInCounter(dhikr)
    }

    fun editDhikrInForm(dhikr: Dhikr) {
        DhikrFormFragment.newInstance(dhikr).let {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, it, getString(R.string.form_fragment_tag))
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onDhikrListItemLongClicked(dhikr: Dhikr) {
        editDhikrInForm(dhikr)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun displayFragment(id: Int) {
        val fragment = when (id) {
            R.id.nav_counter -> CounterFragment()
            R.id.nav_dhikr_list -> DhikrListFragment()
            R.id.nav_themes -> ThemesFragment()
            else -> CounterFragment()
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frameLayout, fragment)
            .commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        displayFragment(item.itemId)

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
