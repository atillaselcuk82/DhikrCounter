package com.example.DhikrCounter.ui.counter

object InputValidationHelper {

    fun isInputCorrect(name: String, amount: String): Boolean {
        return name.isNotEmpty() && amount.isNotEmpty() && amountIsPositiveDigit(
            amount
        )
    }

    fun amountIsPositiveDigit(amount: String): Boolean {
        amount.toIntOrNull()?.let { return it > 0 } ?: return false
    }
}