package com.example.DhikrCounter.ui.dhirkform

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.example.DhikrCounter.R
import com.example.DhikrCounter.model.Dhikr
import com.example.DhikrCounter.model.DhikrDataManager
import com.example.DhikrCounter.ui.dhikrlist.DhikrListFragment
import kotlinx.android.synthetic.main.fragment_dhikr_form.*
import java.text.SimpleDateFormat
import java.util.*


class DhikrFormFragment : Fragment() {

    private var dhikr: Dhikr? = null

    companion object {
        const val DHIKR_KEY = "DHIKR"

        fun newInstance(dhikr: Dhikr): DhikrFormFragment = DhikrFormFragment().apply {
            arguments = Bundle().apply {
                putParcelable(DHIKR_KEY, dhikr)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dhikr_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        addTextFieldWatchers()

        arguments?.let { args ->
            dhikr = args.getParcelable(DHIKR_KEY)
            dhikr?.let {
                name_et.setText(it.name)
                amount_et.setText(it.amount.toString())
                it.description?.let { dsc -> description_et.setText(dsc) }
                it.alert?.let { alt -> alert_et.setText(alt.toString()) }

                it.date?.let { date ->
                    val formatter = SimpleDateFormat("dd/MM/yyyy")
                    due_date_tv.text = formatter.format(date)
                }
            }
            add_btn.setOnClickListener { if (invalidInput()) toastMsg() else addEditedDhikrToList(view.context) }
        } ?: run {
            add_btn.setOnClickListener { if (invalidInput()) toastMsg() else addDhikrToList(view.context) }
        }
        cancel_btn.setOnClickListener { returnToList() }

        due_date_tv.setOnClickListener {
            pickDate(view)
        }
    }

    private fun dateToElement(stringDate: String, sdf: String) : Int {
        val date = SimpleDateFormat("dd/MM/yyyy").parse(stringDate)
        return SimpleDateFormat(sdf).format(date).toInt()
    }

    private fun pickDate(view: View) {

        val c = Calendar.getInstance()
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)

        if (due_date_tv.text.isNotEmpty()) {
            year = dateToElement(due_date_tv.text.toString(), "yyyy")
            month = dateToElement(due_date_tv.text.toString(), "MM") - 1
            day = dateToElement(due_date_tv.text.toString(), "dd")
        }

        val dpd = DatePickerDialog(view.context, DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
            due_date_tv.text = "$mDay/${mMonth + 1}/$mYear"
        }, year, month, day)
        dpd.datePicker.minDate = System.currentTimeMillis()
        dpd.show()
    }

    private fun addTextFieldWatchers() {
        name_et.validate(Regex("^[a-zA-Z ]+"), getString(R.string.name_error), false)
        amount_et.validate(Regex("^[1-9]\\d*\$"), getString(R.string.amount_error), false)
        alert_et.validate(Regex("^[1-9]\\d*\$"), getString(R.string.alert_error), true)
    }

    private fun EditText.validate(pattern: Regex, errorMsg: String, allowEmpty: Boolean) {
        addTextChangedListener(createWatcher {
            when {
                text.isNullOrEmpty() -> if (!allowEmpty) error = "field cannot be empty"
                !text.matches(pattern) -> error = errorMsg
                else -> error = null
            }
        })
    }

    private fun createWatcher(watcher: (CharSequence?) -> Unit): TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            watcher(s)
        }
    }

    private fun invalidInput(): Boolean {
        return name_et.text.isEmpty() ||
                amount_et.text.isEmpty() ||
                name_et.error != null ||
                amount_et.error != null ||
                alert_et.error != null ||
                due_date_tv.error != null
    }

    private fun toastMsg() {
        Toast.makeText(context, "empty fields or incorrect field input", Toast.LENGTH_SHORT).show()
    }

    private fun addEditedDhikrToList(context: Context) {
        dhikr?.let {
            it.name = name_et.text.toString()
            it.description = description_et.text.toString()
            it.amount = amount_et.text.toString().toInt()
            it.alert = alert_et.text.toString().toIntOrNull()
            val dueDate = due_date_tv.text.toString()
            it.date = if (dueDate == "") null else SimpleDateFormat("dd/MM/yyy").parse(dueDate)
            DhikrDataManager(context).editDhikr(it)
            returnToList()
        }
    }

    private fun addDhikrToList(context: Context) {
        val name = name_et.text.toString()
        val description = description_et.text.toString()
        val amount = amount_et.text.toString()
        val alert = alert_et.text.toString()
        val dueDate = due_date_tv.text.toString()
        val date = if (dueDate == "") null else SimpleDateFormat("dd/MM/yyy").parse(dueDate)

        val dhikr = Dhikr(name, description, amount.toInt(), alert.toIntOrNull(), date)
        DhikrDataManager(context).saveDhikr(dhikr)
        returnToList()
    }

    private fun returnToList() {
        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.frameLayout, DhikrListFragment())
            ?.commit()
    }
}
