package com.example.DhikrCounter.ui.themes

import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.example.DhikrCounter.R
import com.example.DhikrCounter.model.ColorShade
import com.example.DhikrCounter.model.ColorShadesObject.getXmlPrimary
import com.example.DhikrCounter.model.DhikrDataManager
import com.example.DhikrCounter.ui.MainActivity
import kotlinx.android.synthetic.main.theme_items.view.*

class ThemesAdapter(private val colorShadesList: ArrayList<ColorShade>) :
    RecyclerView.Adapter<ThemesAdapter.ViewHolder>() {

    var scrollDirection = ScrollDirection.DOWN

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.theme_items, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = colorShadesList.size

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        holder.bind(colorShadesList[p1])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private lateinit var colorShade: ColorShade

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(colorShade: ColorShade) {
            this.colorShade = colorShade
            val context = itemView.context
            itemView.colorTextView.text = colorShade.color.toUpperCase()
            itemView.shadeTextView.text = colorShade.shade.toUpperCase()
            val cardColorResource = context.resources.getIdentifier(
                getXmlPrimary(colorShade.color, colorShade.shade),
                "color",
                context.packageName
            )
            itemView.themesCardContainer.setBackgroundResource(cardColorResource)
            setTextColors(colorShade.shade)
            animateView(itemView)
        }

        private fun setTextColors(shade: String) {
            val textColor = if (shade != R.string.white_text_color_shade.toString()) Color.WHITE else Color.BLACK
            itemView.colorTextView.setTextColor(textColor)
            itemView.shadeTextView.setTextColor(textColor)
        }

        override fun onClick(view: View) {
            val context = view.context
            DhikrDataManager(context).saveTheme(colorShade)
            context.startActivity(Intent(context, MainActivity::class.java))

        }

        private fun animateView(viewToAnimate: View) {
            if (viewToAnimate.animation == null) {
                val animId =  if (scrollDirection == ScrollDirection.DOWN) R.anim.slide_and_scale_from_bottom else R.anim.slide_and_scale_from_top
                val animation = AnimationUtils.loadAnimation(viewToAnimate.context, animId)
                viewToAnimate.animation = animation
            }
        }
    }

    enum class ScrollDirection {
        UP, DOWN
    }
}


