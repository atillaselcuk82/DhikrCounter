package com.example.DhikrCounter.ui.dhikrlist

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.example.DhikrCounter.R
import com.example.DhikrCounter.model.DhikrDataManager
import com.example.DhikrCounter.model.Dhikr
import kotlinx.android.synthetic.main.dhikr_item.view.*
import java.util.*
import java.text.SimpleDateFormat


class DhikrsAdapter(private val list: MutableList<Dhikr>,
                    val clickListener: DhikrListClickListener,
                    private val itemDragListener: ItemDragListener
) :
    RecyclerView.Adapter<DhikrsAdapter.DhikrViewHolder>(),
    ItemTouchHelperListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DhikrViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.dhikr_item, parent, false)
        return DhikrViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface DhikrListClickListener {
        fun dhikrListItemClicked(dhikr: Dhikr)
        fun dhikrListItemLongClicked(dhikr: Dhikr)
    }

    override fun onBindViewHolder(holder: DhikrViewHolder, p1: Int) {
        holder.bind(list[p1])

        holder.itemView.setOnClickListener {
            clickListener.dhikrListItemClicked(list[p1])
        }

        holder.itemView.setOnLongClickListener {
            clickListener.dhikrListItemLongClicked(list[p1])
            return@setOnLongClickListener true
        }
    }

    override fun onItemMove(recyclerView: RecyclerView, fromPosition: Int, toPosition: Int): Boolean {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(list, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(list, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
        DhikrDataManager(recyclerView.context).saveDhikrs(list)
        return true
    }

    override fun onItemDismiss(viewHolder: RecyclerView.ViewHolder, position: Int) {
        DhikrDataManager(viewHolder.itemView.context).deleteDhikr(list[position])
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class DhikrViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        ItemSelectedListener {

        lateinit var dhikr: Dhikr

        fun bind (dhikr: Dhikr) {
            this.dhikr = dhikr
            itemView.dhikr_title_tv.text = dhikr.name
            itemView.dhikr_amount_tv.text = itemView.resources.getString(R.string.amount, dhikr.amount)
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            val today = formatter.parse(formatter.format(Date()))


            dhikr.date?.let {
                val daysLeft = (it.time - today.time) / 1000 / 60 / 60 / 24

                itemView.dhikr_due_date_tv.text = itemView.resources.getString(R.string.due_date, daysLeft)
                itemView.dhikr_due_date_tv.visibility = View.VISIBLE
            } ?: run {
                itemView.dhikr_due_date_tv.visibility = View.INVISIBLE
            }


            if (dhikr.counted != 0 && dhikr.total != 0) {
                itemView.dhikr_counted_tv.text = itemView.resources.getString(R.string.counted, dhikr.counted)
                itemView.dhikr_counted_tv.visibility = View.VISIBLE
            } else {
                itemView.dhikr_counted_tv.visibility = View.INVISIBLE
            }

            if (dhikr.total != 0) {
                itemView.dhikr_total_tv.text = itemView.resources.getString(R.string.total, dhikr.total)
                itemView.dhikr_total_tv.visibility = View.VISIBLE
            } else {
                itemView.dhikr_total_tv.visibility = View.INVISIBLE
            }

            itemView.handle.setOnTouchListener { _, event ->
                if (event.action == MotionEvent.ACTION_DOWN) {
                    itemDragListener.onItemDrag(this)
                }
                false
            }
        }

        override fun onItemSelected() {
            itemView.dhikrItemContainer.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.selectedItem))
        }

        override fun onItemCleared() {
            itemView.dhikrItemContainer.setBackgroundColor(0)
        }
    }
}