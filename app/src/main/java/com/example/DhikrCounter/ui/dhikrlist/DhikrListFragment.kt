package com.example.DhikrCounter.ui.dhikrlist

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.DhikrCounter.R
import com.example.DhikrCounter.model.DhikrDataManager
import com.example.DhikrCounter.model.Dhikr
import com.example.DhikrCounter.ui.dhirkform.DhikrFormFragment
import kotlinx.android.synthetic.main.fragment_dhikr_list.*

class DhikrListFragment: Fragment(),
    DhikrsAdapter.DhikrListClickListener,
    ItemDragListener {

    private var listener: OnFragmentInteractionListener? = null
    private lateinit var dhikrsDataManager: DhikrDataManager
    private lateinit var adapter : DhikrsAdapter
    private lateinit var itemTouchHelper: ItemTouchHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dhikr_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dhikrListRecyclerView.layoutManager = LinearLayoutManager(context)
        dhikrListRecyclerView.adapter = adapter
        setupItemTouchHelper()

        val dividerWidthInPixels = resources.getDimensionPixelSize(R.dimen.dhikr_list_item_divider_height)
        dhikrListRecyclerView.addItemDecoration(
            ListItemDivider(
                ContextCompat.getColor(
                    context!!,
                    R.color.blackLine
                ), dividerWidthInPixels
            )
        )

        openDhikrForm.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout, DhikrFormFragment(), "DhikrListFragment")
                ?.addToBackStack(null)
                ?.commit()
        }
    }

    override fun dhikrListItemClicked(dhikr: Dhikr) {
        listener?.onDhikrListItemClicked(dhikr)
    }

    override fun dhikrListItemLongClicked(dhikr: Dhikr) {
        listener?.onDhikrListItemLongClicked(dhikr)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
            dhikrsDataManager = DhikrDataManager(context)
            adapter = DhikrsAdapter(
                dhikrsDataManager.readDhikrs().toMutableList(),
                this,
                this
            )
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onItemDrag(viewHolder: RecyclerView.ViewHolder) {
        itemTouchHelper.startDrag(viewHolder)
    }

    interface OnFragmentInteractionListener {
        fun onDhikrListItemClicked(dhikr: Dhikr)
        fun onDhikrListItemLongClicked(dhikr: Dhikr)
    }

    private fun setupItemTouchHelper() {
        itemTouchHelper = ItemTouchHelper(ItemTouchHelperCallback(adapter))
        itemTouchHelper.attachToRecyclerView(dhikrListRecyclerView)
    }
}