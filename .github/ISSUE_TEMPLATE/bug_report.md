**Details**  
Device: <e.g. Samsung Galaxy S7>  
OS: <e.g. Android 7.0>  
Environment: <Test / Acceptance / Release / Prod>  
Version: <Version name+number>  

**Preconditions**
<Preconditions>

**Steps to reproduce**
1. <Step 1>
2. <Step 2>
3. ...

**Expected result**
<Describe the expected behavior / result>

**Actual result**
<Describe the actual behavior / result>
