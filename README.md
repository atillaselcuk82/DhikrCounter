# Digital-Dhikr

Dhikr is an Islamic term that stands for rhythmically repeating the name of Allah (God). This project is an Android app where the user can set and count their dhikrs.

## App contains:
1. Default counter: 
- counter without an upperbound 
- counter with an upperbound to be set including name of dhikr
2. Dhikr list: where you can add dhirks with name and upperbound

## To be implemented:
- save and show data after changing landscape/portrait mode (DONE)
- save added dhikrs (DONE)
- delete added dhikrs
- drag and drop feature to order dhikr list
- add animation to dhikr list
- select dhikrs from list and set them in the counter (DONE)
- correct views in landscape mode (DONE)
- create and implement fragments for user friendly UI on tablets
- create color themes 
- share dhirks on network and track completion
